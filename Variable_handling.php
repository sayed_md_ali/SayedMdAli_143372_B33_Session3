<?php

$var="Sayed Mohammed Ali";// type of variable and length
echo var_dump($var);
echo"<br/>";

//It find the value from anything until to get a character.
$string_with_float="235.55bitm33";
$string_with_float1="1210bitm45.55";
echo floatval($string_with_float);
echo"<br/>";
echo floatval($string_with_float1);
echo"<br/>";

//empty function used
$string_with_no_value='';
$string_with_value='sayed Mohammed ali';
$integer_with_no_value=0;
$integer_with_value=20;

if (empty($string_with_no_value))
{
    echo  '$string_with_no_value'."is empty or 0";
}
else
{
    echo '$string_with_no_value'."is not empty or 0";
}
echo"<br/>";

if (empty($string_with_value))
{
    echo "<br/". '$string_with_value'." is empty or 0";
}
else
{
    echo '$string_with_value'." is not empty or 0 <br/>";
}


if (empty($integer_with_value))
{
    echo "<br/". '$integer_with_value'." is empty or 0";
}
else
{
    echo '$integer_with_value'." is not empty or 0<br/>";
}



// is_array function used

$array_with_variable=array("Sayed Mohammed Ali",143372,"Batch 33");
$array_with_no_variable=array();

if (is_array($array_with_variable))
{
    echo '$array_with_variable'." is not empty array <br/>";
}
else
{
    echo '$array_with_variable'."is empty array <br/>";
}

if (is_array($array_with_no_variable))
{
    echo '$array_with_no_variable'." is not empty array <br/>";
}
else
{
    echo '$array_with_no_variable'."is empty array <br/>";
}


//is null function

$variable_with_value=null; //true means there is a value
$variable_with_no_value=0;
if (is_null($variable_with_value))
{
    echo'$variable_with_null'." has nothing i.e. it is null <br/>";
}
else
{
    echo '$variable_with_null'." has something i.e. it is not null <br/>";
}

if(is_null($variable_with_no_value))
{
    echo "Variable has null <br/>";
}
else
{
    echo "variable has somthing <br/>";
}

//isset function

$set_value=false;
if (isset($set_value))
{
    echo "the variable is set <br/>";
}
else
{
    echo "The variable is unset <br/>";
}



$isset_test_null=null;
if (isset($isset_test_null))
{
    echo "The Variable is set";
}
else
{
    echo "The variable is null or unset <br/>";
}
var_dump(isset($set_value),isset($isset_test_null));
echo "<br/>";

// serilize test and unserialized

$arr=array("Sayed Mohammaed Ali","C121045","International Islamic University Chittagong");
$str=serialize($arr);
echo $str;
echo "<br/>";
$my_previous_value=unserialize($str);
print_r($my_previous_value);
?>
//print_r used

<pre>
<?php
$abc=array ('Name:'=>'Sayed Mohammed Ali', 'Seip Id: '=>'143372','Institute'=>'BITM');
print_r ($abc);
?>
</pre>

<?php
//unset test
$unset_test="Sayed Mohammed Ali";
echo "before unset:".$unset_test;
unset($unset_test);
echo "After unset".$unset_test."<br>";
 ?>

